# Common gems
gem "twitter-bootstrap-rails"
gem 'devise'
gem 'inherited_resources'
gem 'thin', :group => :development
gem 'rspec-rails', :group => [:development, :test]

run 'bundle install'

# Rspec setup
generate 'rspec:install'

# Twitter bootstrap setup
remove_file 'app/views/layouts/application.html.erb'
generate 'bootstrap:install'
generate 'bootstrap:layout application fixed'

# Devise setup
generate 'devise:install'
generate 'devise User'
generate 'devise:views'
generate 'controller welcome'
route "root :to => 'welcome#index'"
create_file 'app/views/welcome/index.html.erb', '<h1>This is the default welcome page</h1>'
remove_file 'public/index.html'
inject_into_class 'app/controllers/application_controller.rb', 'ApplicationController' do
  "  before_filter :authenticate_user!\n"
end

# Migrate dbs
rake 'db:migrate'
rake 'db:migrate', :env => 'test'

# Git
git :init
git :add => '.'
git :commit => '-m "Initial application generated"'
